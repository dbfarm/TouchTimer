#include "MyDefine.h"

#pragma rambank0
uint  Temp_4ms;


#pragma	vector Timer_CTM_isr	@ 0x0c			


void Timer_CTM_isr()
{	
	static unsigned char ledScanValue;
	Temp_4ms++;						// 0.4ms*2500
	if(Temp_4ms>=2500)				// =1s
	{
		Temp_4ms=0;	
		
	}
	
	//////////LED Scan//////////////
	
	switch(ledScanValue)
	{
		case 0:
			if((Key1_flag == 1)&&(Key2_flag == 1))
			{
				_pa4 = _pa3 = _pa7 = 1;
				_pc = 0x01;
				_pa1 = 0;
			}
			else if((Key3_flag == 1)&&(Key2_flag == 1))
			{
				_pa4 = _pa3 = _pa7 = 1;
				_pc = 0x04;
				_pa1 = 0;
			}
			else if(Key2_flag == 1)
			{
				_pa4 = _pa3 = _pa7 = 1;
				_pc = 0x02;
				_pa1 = 0;
			}
			else if((Key3_flag == 1)&&(Key4_flag == 0))
			{
				_pa4 = _pa3 = _pa7 = 1;
				_pc = 0x08;
				_pa1 = 0;
			}
		break;
		case 1:
			if((Key7_flag == 1)&&(Key8_flag == 1))
			{
				_pa1 =_pa3 = _pa7 = 1;
				_pc = 0x01;
				_pa4 = 0;	
			}
			else if((Key7_flag == 1)&&(Key1_flag == 1))
			{
				_pa1 =_pa3 = _pa7 = 1;
						_pc = 0x04;
						_pa4 = 0;
			}
			else if(Key7_flag == 1)
			{
				_pa1 = _pa3 = _pa7 = 1;
				_pc = 0x02;
				_pa4 = 0;
			}
			else if((Key1_flag == 1)&&(Key2_flag==0))
			{
				_pa1 = _pa3 = _pa7 = 1;
				_pc = 0x08;
				_pa4 = 0;
			}
		break;
		case 2:
		if((Key5_flag == 1)&&(Key6_flag == 1))
		{
			_pa1 = _pa4 =_pa7 = 1;
			_pc = 0x01;
			_pa3 = 0;
		}
		else if((Key6_flag == 1)&&(Key8_flag == 1))
		{
			_pa1 = _pa4 =_pa7 = 1;
			_pc = 0x04;
			_pa3 = 0;
		}
		else if(Key6_flag == 1)
		{
			_pa1 = _pa4 = _pa7 = 1;
			_pc = 0x02;
			_pa3 = 0;
		}
		else if((Key8_flag == 1)&&(Key7_flag==0))
		{
			_pa1 = _pa4 = _pa7 = 1;
			_pc = 0x08;
			_pa3 = 0;
		}
		break;
		case 3:
		if((Key5_flag == 1)&&(Key6_flag == 0)&&(Key4_flag == 0))
		{
			_pa1 = _pa4 = _pa3 = 1;
			_pc = 0x01;
			_pa7 = 0;
		}
		break;	
	}
	ledScanValue++;
	if(ledScanValue >= 4)ledScanValue = 0;
}
	
