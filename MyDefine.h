#include "BS83B12A-3.h"
#define uchar unsigned char
#define uint unsigned int


#define Key_ON			1
#define Key_OFF			0

#define LED_ON			1
#define LED_OFF			0


#pragma rambank0
extern bit Key1_flag,Key2_flag,Key3_flag,Key4_flag,Key5_flag,Key6_flag,Key7_flag,Key8_flag;
extern bit K1_PIN_VALUE,K2_PIN_VALUE,K3_PIN_VALUE,K4_PIN_VALUE,K5_PIN_VALUE,K6_PIN_VALUE,K7_PIN_VALUE,K8_PIN_VALUE;
#pragma norambank 


