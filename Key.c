#include "MyDefine.h"

#pragma rambank 0

uchar KEY_VALUE_L;
uchar Key1_Debounce,Key2_Debounce;

bit B4_PIN_VALUE,B5_PIN_VALUE,B6_PIN_VALUE,B7_PIN_VALUE;
bit B4_flag,B5_flag,B6_flag,B7_flag;

bit Key1_flag,Key2_flag,Change_flag;
bit Key_Buzzer_flag;

extern bit KEY_DEBOUNCE_FLAG;
extern bit Start_Work_flag;



void  SCAN_KEY()
{
	if(Start_Work_flag)
	{
		if( (Key_ON==B4_PIN_VALUE) && (B4_flag==0) )
		{
			B4_flag = 1;
			Key_Buzzer_flag = 1;
		}			
		if( (Key_ON==B5_PIN_VALUE) && (B5_flag==0) )
		{
			B5_flag = 1;
			Key_Buzzer_flag = 1;
			if( (A3_Pin!=0) || (C1_Pin==0) )
			{
				if(C2_Pin)
					C2_Pin = 0;
				else
					C2_Pin = 1;
			}			
			
		}
		if( (Key_ON==B6_PIN_VALUE) && (B6_flag==0) )
		{
			B6_flag = 1;
			Key_Buzzer_flag = 1;
			
				if(C1_Pin)
					C1_Pin = 0;
				else
					C1_Pin = 1;
					
			if( (C1_Pin==0) && (A3_Pin==0) )
			{
				C2_Pin = A4_Pin = 0;
			}
			else
			{
				if(C1_Pin)	
					A4_Pin = 1;
			}
			
			
		}	
		if( (Key_ON==B7_PIN_VALUE) && (B7_flag==0) )
		{
			B7_flag = 1;
			Key_Buzzer_flag = 1;
		}
		
		
	
		if( (Key_OFF==B4_PIN_VALUE) && (B4_flag==1) )
		{
			B4_flag = 0;
		}			
		if( (Key_OFF==B5_PIN_VALUE) && (B5_flag==1) )
		{
			B5_flag = 0;
		}
		if( (Key_OFF==B6_PIN_VALUE) && (B6_flag==1) )
		{
			B6_flag = 0;
		}	
		if( (Key_OFF==B7_PIN_VALUE) && (B7_flag==1) )
		{
			B7_flag = 0;
		}
	
	
		if(KEY_DEBOUNCE_FLAG)
		{
			KEY_DEBOUNCE_FLAG = 0;
		//----------------------------按键1---------------------------------
			if( (Key1_Pin==Key_ON) && (Key1_flag==0)  )			//按下
			{
				Key1_Debounce++;
				if(Key1_Debounce==20)
				{
					Key1_flag=1;						
					Key1_Debounce = 0;
					Change_flag = 1;	
				}
			}	
			if( (Key1_Pin==Key_OFF) && (Key1_flag==1) )			//释放
			{
				Key1_Debounce++;
				if(Key1_Debounce>=23)
				{
					Key1_flag=0;		
					Key1_Debounce = 0;
					Change_flag = 1;			
				}
			}
			
		//----------------------------按键2---------------------------------	
			if( (Key2_Pin==Key_ON) && (Key2_flag==0)  )			//按下
			{
				Key2_Debounce++;
				if(Key2_Debounce==20)
				{
					Key2_flag=1;						
					Key2_Debounce = 0;
					Change_flag = 1;		
				}
			}	
			if( (Key2_Pin==Key_OFF) && (Key2_flag==1) )			//释放
			{
				Key2_Debounce++;
				if(Key2_Debounce>=23)
				{
					Key2_flag=0;		
					Key2_Debounce = 0;
					Change_flag = 1;			
				}
			}
			
			if(Change_flag)
			{
				Change_flag = 0;
				if( (Key1_flag==0) && (Key2_flag==0) )
				{
					B0_Pin = B1_Pin = B2_Pin = A7_Pin = 1;
					B3_Pin = 0;
					C0_Pin = C1_Pin = C2_Pin = C3_Pin = 0;
				}
				if( (Key1_flag==1) && (Key2_flag==0) )
				{
					B0_Pin = B1_Pin = B2_Pin = A7_Pin = 1;
					B3_Pin = 0;
					A3_Pin = A4_Pin = C0_Pin = 1;
					C1_Pin = C2_Pin = C3_Pin = 0;
				}
				if( (Key1_flag==0) && (Key2_flag==1) )
				{
					B0_Pin = B1_Pin = B2_Pin = A7_Pin = 0;
					B3_Pin = 1;
					A3_Pin = A4_Pin = C0_Pin = C2_Pin = 1;
					C1_Pin = C3_Pin = 0;
				}
				if( (Key1_flag==1) && (Key2_flag==1) )
				{
					B0_Pin = B1_Pin = B2_Pin = A7_Pin = 1;
					B3_Pin = 1;
					A3_Pin = A4_Pin = C1_Pin = C2_Pin = 1;
					C0_Pin = C3_Pin = 0;
				}
			}						
		}
	}
}
