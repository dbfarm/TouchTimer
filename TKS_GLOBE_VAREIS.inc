; ========================================================================================================================
; * Created by: Holtek Touch Key Workshop, Version 3.0.1.6
; TKS_GLOBE_VAREIS.inc
; 04:02:43 PM Monday, December 15, 2014
; ========================================================================================================================
#include "BS83B12A-3.inc"
#define _BS83B12A_
#define _V413_
#define SystemClock     0
#define IO              0
#define KEY             1
; Keys' attribute & settings ...
#define KEY1            KEY
#define Key1Threshold   16
#define KEY2            KEY
#define Key2Threshold   16
#define KEY3            KEY
#define Key3Threshold   16
#define KEY4            KEY
#define Key4Threshold   16
#define KEY5            KEY
#define Key5Threshold   16
#define KEY6            KEY
#define Key6Threshold   16
#define KEY7            KEY
#define Key7Threshold   16
#define KEY8            KEY
#define Key8Threshold   16
#define KEY9            IO
#define Key9Threshold   16
#define KEY10           IO
#define Key10Threshold  16
#define KEY11           IO
#define Key11Threshold  16
#define KEY12           IO
#define Key12Threshold  16
#define IO_TOUCH_ATTR   00000000000000000000000011111111b
; Components' settings ...
; Global options' settings ...
#define DebounceTimes   5 	; 90mS
#define AutoCalibrationPeriod 7 	; x 8 (500.0mS)
#define HighSensitive   1 	; check
#define MaximumKeyHoldTime 0 	; Disable
#define FastResponse    0 	; uncheck
#define AutoFrequencyHopping 0 	; uncheck
#define OneKeyActive    0 	; uncheck
#define PowerSave       0 	; uncheck
#define NoiseProtect    0 	; uncheck
#define MovingCalibration 0 	; uncheck
#define GlobeOptionA    (DebounceTimes | (AutoCalibrationPeriod << 4))
#define GlobeOptionB    (HighSensitive | (MaximumKeyHoldTime << 4))
#define GlobeOptionC    ((FastResponse << 2) | (AutoFrequencyHopping << 3) | (OneKeyActive << 5) | (PowerSave << 6) | (NoiseProtect << 4) | (MovingCalibration << 7))
