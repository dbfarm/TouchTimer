#include "MyDefine.h"

void Write_EEDATA(uchar addr, uchar data)
{
	_eea = addr;
	_eed = data;
	
	_emi = 0;
	_clrwdt();
	#asm
	set [04].0
	mov a,40h
	mov [03],a	
	set [02].3		
	set [02].2
	
	sz	[02].2
	jmp	$-1
	
	#endasm
	_clrwdt();
	_emi = 1;
	
//	_eec |= 0x08;      
//	_eec |= 0x04;	
//	while(_eec & 0x04)   
	_eec = 0;

}


uchar read_eeprom(uchar addr)
{
	_eea = addr;
//	_eec |=0x02;    
//	_eec |=0x01; 

	_emi = 0;
	_clrwdt();
	#asm
	set [04].0
	mov a,40h
	mov [03],a	
	set [02].1		
	set [02].0
	
	sz	[02].0
	jmp	$-1
	#endasm
	_clrwdt();	
	_emi = 1;
    
//	while(_eec & 0x01)
//		_clrwdt();         
	_eec = 0;           

	return _eed;
}
