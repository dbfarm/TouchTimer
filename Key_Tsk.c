#include "MyDefine.h"

#pragma rambank 0

extern bit doKey1_flag,doKey2_flag,doKey3_flag,doKey4_flag;

#pragma norambank

void DO_KEY()
{
	if(doKey1_flag)
	{
		doKey1_flag = 0;
		if(KeyH_Status_flag)
		{
			LED1_H_Pin = 0;
			backup_LEDH_flag = 0;		
		}
		else
		{
			LED1_H_Pin = 1;
			backup_LEDH_flag=1;
		}
	}	

}